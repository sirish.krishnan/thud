/*
 *     <>=======()
 *    (/\___   /|\\          ()==========<>_
 *          \_/ | \\        //|\   ______/ \)
 *            \_|  \\      // | \_/
 *              \|\/|\_   //  /\/
 *               (oo)\ \_//  /
 *              //_/\_\/ /  |
 *             @@/  |=\  \  |
 *                  \_=\_ \ |
 *                    \==\ \|\_ 
 *   THUD!         __(\===\(  )\
 *   ~~~~~        (((~) __(_/   |
 *                     (((~) \  /
 *                     ______/ /
 *                     '------'
 *
 */


INTRODUCTION
============

"Thud!" is a simple compiler which demonstrates dynamic scoping of variables 
for programs written in C, it is built using the Clang-LLVM infrastructure.
