/* ============================================================== */

/*
 *     <>=======()
 *    (/\___   /|\\          ()==========<>_
 *          \_/ | \\        //|\   ______/ \)
 *            \_|  \\      // | \_/
 *              \|\/|\_   //  /\/
 *               (oo)\ \_//  /
 *              //_/\_\/ /  |
 *             @@/  |=\  \  |
 *                  \_=\_ \ |
 *                    \==\ \|\_ 
 *   THUD!         __(\===\(  )\
 *   ~~~~~        (((~) __(_/   |
 *                     (((~) \  /
 *                     ______/ /
 *                     '------'
 *
 */

/* ==============================================================
 *
 *  filename : ThudAPI.cpp
 *  purpose  : Contains all supporting class & function definitions   
 *             for supporting functionality required for Thud.
 *
 * ============================================================== */


/* ================
 * Include file(s) 
 * ================
 */

#include <iostream>
#include <list>
#include <regex>
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "clang/Tooling/Tooling.h"
#include "llvm/Support/CommandLine.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Rewrite/Frontend/Rewriters.h"
#include "ThudAPI.h"
#include "ThudErrors.h"
using namespace std;

/* ============================================================== */

/* ===========================
 * Member function definitions 
 * for class "thud_decl_base"
 * ===========================
 */

/* ==============================================================
 *
 * name     : thud_decl_base()
 *
 * purpose  : Constructor definitions for the class.        
 *
 * ============================================================== */

thud_decl_base::thud_decl_base()   // Constructor
{
    name = "";
}

thud_decl_base::thud_decl_base(const thud_decl_base &copyin)   // Copy constructor to handle pass by value.
{                             
    name = copyin.name;
    size = copyin.size;
    decl = copyin.decl;
    location = copyin.location;
}

thud_decl_base::thud_decl_base(std::string attr_name, int attr_size, void *attr_decl, void* attr_loc) 
{                             
    name = attr_name;
    size = attr_size;
    decl = attr_decl;
    location = attr_loc;
}

int thud_decl_base::thud_update_attr_details(std::string attr_name, int attr_size, void *attr_decl, void *attr_loc)
{
    name = attr_name;
    size = attr_size;
    decl = attr_decl;
    location = attr_loc;
    return THUD_SUCCESS;
}

int thud_decl_base::get_size(int )
{
    return this->size;
}

ostream &operator<<(ostream &output, const thud_decl_base &var_decl)
{
    output << var_decl.name << endl;
    return output;
}

thud_decl_base& thud_decl_base::operator=(const thud_decl_base &rhs)
{
    this->name = rhs.name;
    this->size = rhs.size;
    return *this;
}

int thud_decl_base::operator==(const thud_decl_base &rhs) const
{
    if( this->name != rhs.name) return THUD_SUCCESS;
    return THUD_FAILURE;
}

/* This function is required for built-in STL list functions like sort */
int thud_decl_base::operator<(const thud_decl_base &rhs) const
{
    if( this->name < rhs.name ) return THUD_SUCCESS;
    return THUD_FAILURE;
}

/* ==================================
 * List-related function definitions 
 * ==================================
 */

/* ==============================================================
 *
 * name     : thud_retrieve_attr_block()
 *
 * purpose  : Retrieves pointer to an instance of the class based 
 *            on the string key "attr_name" from a list of class
 *            instances.
 *
 * ============================================================== */

thud_decl_base* thud_retrieve_attr_block(list<thud_decl_base> &L, std::string attr_name)
{
    list<thud_decl_base>::iterator i;

    for(i=L.begin(); i != L.end(); ++i) //cout << (*i).name << " "; // print member
    {
        if (i->name == attr_name)
            return &(*i);
    }
    return NULL;
}

/* ==============================================================
 *
 * name     : thud_display_attr_details()
 *
 * purpose  : Used to display all attribute details of an instance
 *            of this class (For debugging purposes).
 *
 * ============================================================== */

void thud_display_attr_details(list<thud_decl_base> &L)
{
    list<thud_decl_base>::iterator i; 
    list<thud_decl_base>::iterator j; 
    list<thud_decl_base> L2;

    for(i=L.begin(); i != L.end(); ++i) //cout << (*i).name << " "; // print member
    {
        thud_debug("Name : " << (*i).name);
        thud_debug("Size : " << (*i).size);
        L2 = (*i).sublist;
        thud_debug("Subattr 1 details : ");
        for(j=((*i).sublist).begin(); j != ((*i).sublist).end(); j++) 
        {
            thud_debug("    Subname : " << (*j).name); // print with overloaded operator
            thud_debug("    SubSize : " << (*j).size); // print with overloaded operator
        }
        thud_debug("Subattr 2 details : ");
        for(j=((*i).sublist2).begin(); j != ((*i).sublist2).end(); j++) 
        {
            thud_debug("    Subname : " << (*j).name); // print with overloaded operator
            thud_debug("    SubSize : " << (*j).size); // print with overloaded operator
        }
    }
    thud_debug("\n\n");
}

/* ==============================================================
 *
 * name     : thud_update_list_unique_with_decl()
 *
 * purpose  : Used to update a list with latest data (given in
 *            'ptr' argument) only if the instance of the class
 *            with key "attr_name" is present in the list.
 *
 * ============================================================== */

int thud_update_list_unique_with_decl(list<thud_decl_base> &L, std::string attr_name, int attr_size, void *ptr)
{
    list<thud_decl_base>::iterator i;
    thud_decl_base *temp = NULL;

    temp = thud_retrieve_attr_block(L, attr_name);

    if (NULL != temp)
    {
        temp->thud_update_attr_details(attr_name,attr_size, ptr, ptr);
        return THUD_SUCCESS;
    }

    return THUD_DUPLICATE_ENTRY;
}

/* ==============================================================
 *
 * name     : thud_add_list_unique_with_decl()
 *
 * purpose  : Used to add to a list only if the key "attr_name" is
 *            not already included in one of the instances of the
 *            class in the list. The data added is usually a
 *            pointer to a type derived from clang::Decl base class
 *            along with additional details like size / location etc.
 *
 * ============================================================== */

int thud_add_list_unique_with_decl(list<thud_decl_base> &L, std::string attr_name, int attr_size, void *ptr)
{
    list<thud_decl_base>::iterator i;
    thud_decl_base *temp = NULL;

    temp = thud_retrieve_attr_block(L, attr_name);

    if (NULL == temp)
    {
        temp = new thud_decl_base;
        temp->thud_update_attr_details(attr_name,attr_size, ptr, ptr);
        L.push_back(*temp);
        return THUD_SUCCESS;
    }

    return THUD_NODE_NOT_FOUND;
}

/* ==============================================================
 *
 * name     : thud_add_to_list_notunique()
 *
 * purpose  : Used to add an instance of the class to a list,
 *            the key "attr_name" need not be unique for insertion.
 *
 * ============================================================== */

int thud_add_to_list_notunique(list<thud_decl_base> &L, std::string attr_name, void *data)
{
    list<thud_decl_base>::iterator i;
    thud_decl_base *temp = NULL;

    temp = new thud_decl_base;
    temp->thud_update_attr_details(attr_name,0, data, data);
    L.push_back(*temp);
    return THUD_SUCCESS;

    return THUD_DUPLICATE_ENTRY;
}

/* ==============================================================
 *
 * name     : thud_add_to_list_unique()
 *
 * purpose  : Used to add an instance of the class to a list,
 *            the key "attr_name" has to be unique; this function
 *            does not store clang::Decl info.
 *
 * ============================================================== */

int thud_add_to_list_unique(list<thud_decl_base> &L, std::string attr_name, int attr_size)
{
    list<thud_decl_base>::iterator i;
    thud_decl_base *temp = NULL;

    temp = thud_retrieve_attr_block(L, attr_name);

    if (NULL == temp)
    {
        temp = new thud_decl_base;
        temp->thud_update_attr_details(attr_name,attr_size, NULL, NULL);
        L.push_back(*temp);
        return THUD_SUCCESS;
    }

    return THUD_DUPLICATE_ENTRY;
}

/* ==============================================================
 *
 * name     : thud_add_to_list_and_sublist_unique()
 *
 * purpose  : Used to add an instance of the class to a list,
 *            as well as a sublist; for keeping track of various
 *            variables declared in the program and the max size
 *            for each unique variable name.
 *
 * ============================================================== */

int thud_add_to_list_and_sublist_unique(list<thud_decl_base> &L, std::string attr_name, std::string attr_type, data_type_details type_details)
{
    list<thud_decl_base>::iterator i;
    thud_decl_base *temp_parent = NULL;
    thud_decl_base *temp_child = NULL;

    thud_debug("Adding variable name to list : " << attr_name);
    thud_debug("Data type is " << type_details.datatype);
    thud_debug("Array size is " << type_details.array_size);
    thud_debug("Total size is " << type_details.total_size);

    /* Check if parent object of particular variable's name exists,
     * if not, create it 
     */
    temp_parent = thud_retrieve_attr_block(L, attr_name);

    if (NULL == temp_parent)
    {
        thud_debug("temp_parent was null, creating a new parent for varname " << attr_name);
        temp_parent = new thud_decl_base;
        temp_parent->thud_update_attr_details(attr_name,type_details.total_size, NULL, NULL);
        L.push_back(*temp_parent);
        temp_parent = thud_retrieve_attr_block(L, attr_name);
    }

    /* Check if child type of particular variable's name already exists,
     * if not, create it and add it to the parent's sublist
     */

    temp_child = thud_retrieve_attr_block((*temp_parent).sublist, attr_type); 

    if (NULL == temp_child)
    {
        thud_debug("temp_child was null, creating a new parent for type " << attr_type);
        temp_child = new thud_decl_base;
        if (type_details.array_size == 0)
            temp_child->thud_update_attr_details(attr_type,type_details.total_size, NULL, NULL);
        else
            temp_child->thud_update_attr_details(attr_type,type_details.array_size, NULL, NULL);
        ((*temp_parent).sublist).push_back((*temp_child));

        /*
         * Update the parent's size parameter to max size of its children
         */
        if ((temp_parent -> size) < type_details.total_size)
            temp_parent -> size = type_details.total_size;

        return THUD_SUCCESS;
    }

    return THUD_DUPLICATE_ENTRY;
}

/* ============================================================== */

/* ==========================
 * Misc function definitions 
 * ==========================
 */

/* ==============================================================
 *
 * name     : thud_get_type_of_variable()
 *
 * purpose  : Assigns an enum value for each variable type supported
 *            by THUD. This API is DEPRECATED.
 *
 * ============================================================== */

int thud_get_type_of_variable(std::string attr_type)
{
    regex intreg("int");
    regex floatreg("float");
    regex structreg("struct");

    if (regex_search(attr_type.c_str(), intreg))
        return THUD_INT;
    else if (regex_search(attr_type.c_str(), floatreg))
        return THUD_FLOAT;
    else if (regex_search(attr_type.c_str(), structreg))
        return THUD_STRUCT;

    return THUD_INVALID;
}

/* ==============================================================
 *
 * name     : thud_get_array_size()
 *
 * purpose  : Used to get the size of an array from the declaration 
 *            string by using regular expression matching.
 *
 * ============================================================== */

int thud_get_array_size(std::string attr_type)
{
    regex arrayreg("[a-z]*[0-9]+");
    int match_found = 0;
    std::smatch match;
    int array_size = 0;

    /* Check if this is an array / not */
    match_found = regex_search(attr_type, arrayreg);
    thud_debug("Search result is " << match_found);
    if (match_found)
    {
        sregex_iterator it(attr_type.begin(), attr_type.end(), arrayreg);
        sregex_iterator it_end;

        while(it != it_end) {
            match = *it;
            ++it;
        }   
        thud_debug("Size of data array is " << match.str());
        array_size = stoi(match.str());
    }
    else
    {
        array_size = 0;
    }
    return array_size;
}

/* ==============================================================
 *
 * name     : thud_get_size_of_struct()
 *
 * purpose  : Used to get the size of a struct from its 
 *            declaration.
 *
 * ============================================================== */

int thud_get_size_of_struct( std::string attr_type, clang::QualType qt )
{
    regex structreg("struct");
    int unitsize = 0;
    int arraysize = 0;
    
    /* Check if this is a struct / not */
    if (regex_search(attr_type, structreg))
    {
        /* Yes, this is a struct */
        const clang::Type *type = qt.getTypePtrOrNull();
        const clang::RecordType *recordtype = NULL;
        clang::RecordDecl *recorddecl = NULL;
        thud_debug("All hail Ramana ! A struct has been found !!");
        thud_debug("Its name is " << qt.getAsString());
        if (NULL == type)
        {
            thud_debug("Type ptr was NULL !");
        }
        else
        {
            recordtype = type->getAsStructureType();
            if (NULL == recordtype)
            {
                thud_debug("Record pointer is NULL ! Try to derive its struct handle from base type");
                type = type -> getBaseElementTypeUnsafe();
                if (NULL == type)
                {
                    thud_debug("Could not get the base type of this variable");
                }
                else
                {   
                    recordtype = type->getAsStructureType();
                    thud_debug("Base element type successfully derived !");
                    if (NULL == recordtype)
                    {
                        thud_debug("Recordtype was NULL!");
                    }
                    else
                    {
                        thud_debug("Recordtype was successfully obtained in the 2nd pass!");
                    }
                }
            }
            /* If the 1st try itself (where the variable was not an array of struct type)
             * or 2nd try (where variable was an array of struct type and we tried to 
             * derive its base type from the pointer handle ) succeeded, continue with the
             * rest of data type size calculation
             */
            if (recordtype != NULL)
            {
                thud_debug("All is well, we're back in business !");
                recorddecl = recordtype -> getDecl();
                thud_debug("Dumping the record decl :");
                for (const auto *fi : recorddecl->fields())
                {
                    if (fi->isUnnamedBitfield()) continue;
                    clang::QualType qt = fi -> getType();
                    std::string subtype = qt.getAsString(); 
                    thud_debug("Name of field is " << fi->getName());
                    thud_debug("Type of field is " << subtype);

                    arraysize = thud_get_array_size(subtype);

                    if (arraysize == 0)
                        arraysize = 1; /* Muhahaha ! :D */

                    if (thud_get_type_of_variable(subtype) == THUD_INT || \
                        thud_get_type_of_variable(subtype) == THUD_FLOAT )
                    {
                        unitsize += sizeof(int) * arraysize;
                    }
                    else if (thud_get_type_of_variable(subtype) == THUD_STRUCT)
                    {
                        unitsize += thud_get_size_of_struct(subtype, qt) * arraysize;
                    }
                }
            }
            else
            {
                unitsize = 0;
            }
        }
    }
    else
    {
        /* This is not a struct */
        unitsize = 0;
    }
    return unitsize;
}

/* ==============================================================
 *
 * name     : thud_get_size_of_datatype()
 *
 * purpose  : Used to populate a structure instance with all size
 *            related info like datatype unit size, array size of
 *            the variable as a whole and the total size.
 *
 * ============================================================== */

data_type_details thud_get_size_of_datatype (std::string attr_type, clang::QualType &qt)
{
    int unitsize = 0;
    data_type_details type_details = {0,0,0};

    unitsize += thud_get_size_of_struct( attr_type, qt );

    type_details.array_size = thud_get_array_size(attr_type);

    type_details.datatype = thud_get_type_of_variable(attr_type);

    switch (type_details.datatype)
    {
        case THUD_INT:
            unitsize = sizeof(int);
            break;
        case THUD_FLOAT:
            unitsize = sizeof(float);
            break;
        case THUD_STRUCT:
            /* Do nothing ! Unitsize would be 
             * handled for separately as above
             */
            break;
        default:
            unitsize = THUD_ZERO_SIZE;
            break;
    }

    thud_debug("Unit size is " << unitsize);

    if (type_details.array_size != 0)
    {
        type_details.datatype++; // Convert it into an array type
        type_details.total_size = (unitsize * type_details.array_size);
    }
    else
    {
        type_details.total_size = unitsize;
    }

    thud_debug("Total size is " << type_details.total_size);

    return type_details;
}

/* ============================================================== */

/* ================
 * Misc function(s) 
 * ================
 */

/* ==============================================================
 *
 * name     : thud_decl2str()
 *
 * purpose  : Translates a clang::Decl instance to its        
 *            equivalent string declaration.
 *
 * ============================================================== */

std::string thud_decl2str(clang::Rewriter &TheRewriter, clang::Decl *d) 
{
    clang::LangOptions lopt;
    clang::SourceLocation b(d->getLocStart()), _e(d->getLocEnd());
    clang::SourceLocation e(clang::Lexer::getLocForEndOfToken(_e, 0, TheRewriter.getSourceMgr(), lopt));
    return std::string(TheRewriter.getSourceMgr().getCharacterData(b),
            TheRewriter.getSourceMgr().getCharacterData(e)-TheRewriter.getSourceMgr().getCharacterData(b));
}

/* ==============================================================
 *
 * name     : thud_replace_invalid_text()
 *
 * purpose  : When there's an invalid type mismatch, this function
 *            is invoked to replace the entire statement with a print
 *            that intimates the same to the user & exits the user's
 *            program.
 *
 * ============================================================== */

int thud_replace_invalid_text(clang::Rewriter &TheRewriter, clang::SourceLocation *SL) 
{
    clang::LangOptions lopt;
    std::string start_of_madness;
    int start_offset = 0;
    int end_offset = 0;
    clang::Rewriter::RewriteOptions removeOpts;
    removeOpts.IncludeInsertsAtBeginOfRange = false;
    removeOpts.IncludeInsertsAtEndOfRange = false;
    removeOpts.RemoveLineIfEmpty = true;
    StringRef outName = TheRewriter.getSourceMgr().getFilename(*SL);
    std::error_code OutErrorInfo;
    llvm::raw_fd_ostream outFile(outName, OutErrorInfo, llvm::sys::fs::F_None);
    std::stringstream decl_text;

    do {
        clang::SourceLocation b(SL -> getLocWithOffset(start_offset));
        start_of_madness = std::string(TheRewriter.getSourceMgr().getCharacterData(b),1);
        start_offset--;
    } while (start_of_madness[0] != ';');

    do {
        clang::SourceLocation b(SL -> getLocWithOffset(end_offset));
        start_of_madness = std::string(TheRewriter.getSourceMgr().getCharacterData(b),1);
        end_offset++;
    } while (start_of_madness[0] != ';');

    start_of_madness = std::string(TheRewriter.getSourceMgr().getCharacterData(SL->getLocWithOffset(start_offset+2))\
                                   ,(end_offset-start_offset-2)); 
    thud_debug("String was <" << start_of_madness << ">");
    TheRewriter.RemoveText(SL -> getLocWithOffset(start_offset+2), (end_offset-start_offset-2), removeOpts);

    decl_text.str("{printf(\"Sorry! Invalid type!\"); exit(1);}\n");
    TheRewriter.InsertText(SL -> getLocWithOffset(start_offset+2), decl_text.str(), true, false); 

    /* Write to the original file */
    thud_debug("Writing to file finally : " << TheRewriter.getSourceMgr().getFilename(*SL).data());
    TheRewriter.getEditBuffer(TheRewriter.getSourceMgr().getMainFileID())
        .write(outFile);

    thud_debug("Displaying final modified source file :");
    TheRewriter.getEditBuffer(TheRewriter.getSourceMgr().getMainFileID())
        .write(llvm::outs());

    return 0;
}

/* ==============================================================
 *
 * name     : thud_callexpr2str()
 *
 * purpose  : This function converts the CallExpr clang type pointer
 *            to its equivalent string form 
 *
 * ============================================================== */

int thud_callexpr2str(clang::Rewriter &TheRewriter, clang::CallExpr *d) 
{
    clang::LangOptions lopt;
    std::string start_of_madness;
    int offset = 0;
    clang::SourceLocation b_final;
    //clang::SourceLocation e(clang::Lexer::getLocForEndOfToken(d->getLocEnd(), 0, TheRewriter.getSourceMgr(), lopt));

    do {
        clang::SourceLocation b(d->getLocStart().getLocWithOffset(offset));
        start_of_madness = std::string(TheRewriter.getSourceMgr().getCharacterData(b),1);
        offset--;
        b_final = b;
    } while (start_of_madness[0] != ';');

    return (offset+2);
}

/* ==============================================================
 *
 * name     : test_linked_list()
 *
 * purpose  : This function is used to test the thud_decl_base class
 *            functionalities & C++ STL list API
 *
 * ============================================================== */

int test_linked_list()
{
    list<thud_decl_base> L;
    list<thud_decl_base>::iterator i;

    thud_add_to_list_unique(L, "Ramakrishna", 2000);
    thud_add_to_list_unique(L, "Vivekananda", 1000);
    thud_add_to_list_unique(L, "Ramana", 2000);
    thud_add_to_list_unique(L, "Muruganar", 1000);
    thud_decl_base *obj = NULL;
    obj = thud_retrieve_attr_block(L, "Vivekananda");
    thud_add_to_list_unique(obj->sublist,"float",4);
    thud_add_to_list_unique(obj->sublist,"int [10]",40);
    obj = thud_retrieve_attr_block(L, "Ramana");
    thud_add_to_list_unique(obj->sublist,"int",4);
    thud_add_to_list_unique(obj->sublist,"float [10]",40);
    thud_display_attr_details(L);

    cout << "Remove where name='Muruganar': " << endl;
    for(i=L.begin(); i != L.end(); )       // Don't increment iterator here
        if( (*i).name == "Muruganar" ) i = L.erase(i);  // Returns iterator to the next item in the list
        else ++i;                          // Increment iterator here
    for(i=L.begin(); i != L.end(); ++i) cout << *i; // print with overloaded operator
    cout << endl;

    return 0;
}

/* ============================================================== */
