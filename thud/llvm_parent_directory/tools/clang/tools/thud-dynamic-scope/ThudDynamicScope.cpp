/* ============================================================== */

/*
 *     <>=======()
 *    (/\___   /|\\          ()==========<>_
 *          \_/ | \\        //|\   ______/ \)
 *            \_|  \\      // | \_/
 *              \|\/|\_   //  /\/
 *               (oo)\ \_//  /
 *              //_/\_\/ /  |
 *             @@/  |=\  \  |
 *                  \_=\_ \ |
 *                    \==\ \|\_ 
 *   THUD!         __(\===\(  )\
 *   ~~~~~        (((~) __(_/   |
 *                     (((~) \  /
 *                     ______/ /
 *                     '------'
 *
 */

/* ==============================================================
 *
 *  filename : ThudDynamicScope.cpp
 *  purpose  : Contains the state machine to transform a C-source 
 *             to an equivalent C-source with dynamic scope for
 *             the variables defined.
 *  
 * ============================================================== */

/* ================
 * Include file(s) 
 * ================
 */

#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "clang/Tooling/Tooling.h"
#include "llvm/Support/CommandLine.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Rewrite/Frontend/Rewriters.h"
#include "ThudAPI.h"
#include "ThudErrors.h"
#include <sstream>
#include <regex>
using namespace clang;
using namespace clang::tooling;
using namespace llvm;

/* ============================================================== */

/* =======================
 * Global & static symbol 
 * definitions
 * =======================
 */

/* This has a list of all variable names seen in the program
 * along with a sublist consisting of the unique types seen
 * with each instance of a variable's name
 */
 static list<thud_decl_base> thud_var_name_list;
 static list<thud_decl_base> thud_undeclared_var_name_list;
 static list<thud_decl_base> thud_undeclared_var_name_list_pruned;
 static list<thud_decl_base> thud_invalid_type_list;
 static list<thud_decl_base> thud_func_list;
 static list<thud_decl_base> thud_global_var_list;
 static int thud_current_pass = THUD_PASS_STATIC_STORAGE_CREATION;
 static bool thud_has_global = false;
 static bool premature_exit = false;
 static bool handled_all_undeclared_vars = false;

/* ============================================================== */

/* =======================
 * Class & member function 
 * definitions
 * =======================
 */

/* ==============================================================
 *
 * name     : thud_update_source_with_transformations()
 *
 * purpose  : This function writes all source2source transformations
 *            to the original file
 *
 *
 * ============================================================== */

static void thud_update_source_with_transformations( Rewriter &RW )
{
    std::error_code OutErrorInfo;
    SourceLocation target_source_loc = RW.getSourceMgr().getLocForStartOfFile(RW.getSourceMgr().getMainFileID());
    StringRef outName = RW.getSourceMgr().getFilename(target_source_loc);
    llvm::raw_fd_ostream outFile(outName, OutErrorInfo, llvm::sys::fs::F_None);
    Rewriter::RewriteOptions removeOpts;
    removeOpts.IncludeInsertsAtBeginOfRange = false;
    removeOpts.IncludeInsertsAtEndOfRange = false;
    removeOpts.RemoveLineIfEmpty = true;

    /* Write to the original file */
    thud_debug("Writing to file : " << RW.getSourceMgr().getFilename(target_source_loc).data());
    RW.getEditBuffer(RW.getSourceMgr().getMainFileID())
        .write(outFile);

#if (THUD_DBG == 1)
    thud_debug("Displaying modified source file :");
    RW.getEditBuffer(RW.getSourceMgr().getMainFileID())
        .write(llvm::outs());
#endif
}

/* ============================================================== */

/* =========================
 * Diagnostics Handler Class
 * =========================
 */

class ThudDiagnosticConsumer : public clang::DiagnosticConsumer 
{
    public:
        Rewriter &TheRewriter;

        explicit ThudDiagnosticConsumer (Rewriter &R):TheRewriter(R)
    {
    }


/* ==============================================================
 *
 * name     : HandleDiagnostic()
 *
 * purpose  : This function populates a list of all variables not
 *            declared in the program. It also keeps track of type
 *            mismatch errors in the program.
 *
 *
 * ============================================================== */

        void HandleDiagnostic(clang::DiagnosticsEngine::Level DiagLevel,
                const clang::Diagnostic& Info) override 
        {
            llvm::SmallVector<char, 128> message;
            Info.FormatDiagnostic(message);
            list<thud_decl_base>::iterator j; 
            //FunctionDecl *func_decl = NULL;

            /* Don't start snooping for undeclared variables unless
             * it's the THUD_PASS_PRUNE_UNDECLARED_VARIABLES compiler pass
             */

            if (thud_current_pass == THUD_PASS_PRUNE_UNDECLARED_VARIABLES)
            {
                for (unsigned I = 0, N = Info.getNumArgs(); I != N; ++I) 
                {
                    thud_debug("Iterating through InfoArgs, thud_current_pass is " << thud_current_pass);
                    if (Info.getArgKind(I) == DiagnosticsEngine::ak_declarationname)
                    {
                        SourceLocation *SL = new SourceLocation (Info.getLocation());
                        thud_debug("Found an undeclared variable!");
                        DeclarationName dn = DeclarationName::getFromOpaqueInteger(Info.getRawArg(I));
                        std::string undeclared_varname = dn.getAsString();
                        thud_debug("Name of symbol is " << undeclared_varname << "\n");
                        thud_debug("Is this an identifier ? " << dn.isIdentifier() << "\n");

                        /* If it's an identifier, add it to the list of undeclared variables */
                        if (dn.isIdentifier())
                        {
                            thud_debug( "Undeclared identifier's name is : " << undeclared_varname << "\n");
                            //thud_add_list_unique_with_decl(thud_undeclared_var_name_list, undeclared_varname, 0, (void *)SL);
                            thud_add_to_list_notunique(thud_undeclared_var_name_list, undeclared_varname,SL);
                        }
                        else
                        {
                            thud_error( "Token pointer is NULL \n" );
                        }
                    }
                }
            }
            else if (thud_current_pass == THUD_PASS_HANDLE_INVALID_TYPE_ASSIGNMENTS)
            {
                std::string msg;
                regex reg_array("array");
                regex reg_struct("structure");

                for (char c : message)
                    msg.push_back(c);

                if (!(regex_search(msg, reg_array) ||\
                            regex_search(msg, reg_struct)))
                    return;

                thud_debug("Message received was " << message);
                if (Info.getNumArgs() == 0)
                {
                    SourceLocation *SL = new SourceLocation (Info.getLocation());
                    thud_debug("Number of InfoArgs is 0 ");
                    //thud_replace_invalid_text(TheRewriter, SL);
                    thud_add_to_list_notunique(thud_invalid_type_list,"invalid",SL);
                }
                for (unsigned I = 0, N = Info.getNumArgs(); I != N; ++I) 
                {
                    SourceLocation *SL = new SourceLocation (Info.getLocation());
                    thud_debug("Number of InfoArgs is not 0 ");
                    thud_debug("Iterating through InfoArgs, thud_current_pass is " << thud_current_pass);
                    thud_debug("Info type is " << Info.getArgKind(I));
                    //thud_replace_invalid_text(TheRewriter, SL);
                    thud_add_to_list_notunique(thud_invalid_type_list,"invalid",SL);
                }
            }
            else
            {
                // Do nothing
                thud_debug("How about some tea... ?");
            }
        }
};

/* ============================================================== */

/* =============================================
 * Thud! Compiler State Machine(s) start(s) here  
 * =============================================
 */

class ThudPassOneVisitor:public RecursiveASTVisitor < ThudPassOneVisitor >
{
    public:

        explicit ThudPassOneVisitor (ASTContext * Context, Rewriter &R):TheRewriter(R)
        {
            auto& DE = Context->getDiagnostics();
            //DE.setClient(new ThudDiagnosticConsumer(), /*ShouldOwnClient=*/true);
            DE.setClient(new ThudDiagnosticConsumer(R), /*ShouldOwnClient=*/true);
        }

/* ==============================================================
 *
 * name     : VisitCallExpr()
 *
 * purpose  : Virtual function overridden to handle save and restore
 *            of variable context before and after a function call.
 *
 * ============================================================== */

        bool VisitCallExpr (CallExpr *CE)
        {
            list<thud_decl_base>::iterator j; 
            std::string callexpr_name;
            unsigned int call_expr_raw_loc = (CE -> getLocStart()).getRawEncoding(); 
            FunctionDecl *func_decl = NULL;

            if (thud_current_pass == THUD_PASS_SAVE_RESTORE_VARIABLE_CONTEXT)
            {
                thud_debug( "Visited Call expression" );
                thud_debug("Call expr starts @ " << (CE -> getLocStart()).getRawEncoding());
                thud_debug("Call expr ends @ " << (CE -> getLocEnd()).getRawEncoding());
                FunctionDecl *FD = CE->getDirectCallee();

                if (FD != NULL)
                {
                    callexpr_name = FD -> getNameAsString();
                    thud_debug("Name of function called was " << callexpr_name);

                    if (("memcpy" == callexpr_name) || ("printf" == callexpr_name) || \
                            ("exit" == callexpr_name) || ("strncmp" == callexpr_name) || \
                            ("strcpy" == callexpr_name) || ("strncpy" == callexpr_name) || \
                            ("strcmp" == callexpr_name))
                    {
                        thud_debug("This function is passé, shall skip it !");
                        return true;
                    }
                    else
                    {
                        /* Figure out who called this function and add the callee to the
                         * caller's list
                         */
                        for(j=thud_func_list.begin(); j != thud_func_list.end(); ++j) 
                        {
                            func_decl = (FunctionDecl *) (*j).location;
                            unsigned int func_raw_loc_begin = (func_decl -> getLocStart()).getRawEncoding(); 
                            unsigned int func_raw_loc_end = (func_decl -> getLocEnd()).getRawEncoding(); 
                            thud_debug("The function " << (*j).name << " lies between" << \
                                    func_raw_loc_begin << " and " << func_raw_loc_end );
                            if ( call_expr_raw_loc > func_raw_loc_begin && call_expr_raw_loc < func_raw_loc_end)
                            {
                                thud_debug("Yippy! Found the CallExpr " << callexpr_name << \
                                        " inside function " << (*j).name);
                                thud_debug("Adding it to the FuncDecl's list of CallExprs");
                                thud_add_to_list_notunique((*j).sublist, callexpr_name, (void *)CE);
                                break;
                            }
                        }
                    }
                }
                else
                {
                    thud_error("Call was not a function");
                    return true;
                }
            }
            return true;
        }

/* ==============================================================
 *
 * name     : VisitFunctionDecl()
 *
 * purpose  : Virtual function overridden to update a list of all
 *            functions declared in the program (useful for static)
 *            storage allocation in Pass 1.
 *
 * ============================================================== */

        bool VisitFunctionDecl (FunctionDecl *Declaration)
        {
            /* Ensure that we build the list of FunctionDecls 
             * only in the 1st pass 
             */
            if (thud_current_pass == THUD_PASS_STATIC_STORAGE_CREATION)
            {
                thud_debug("This is the THUD_PASS_STATIC_STORAGE_CREATION pass, adding FunctionDecl info to a list");
                /* Add this Declaration to list of all functions traversed so far */
                thud_debug("Pointer location of Declaration is " << Declaration << "\n");
                thud_add_list_unique_with_decl(thud_func_list, Declaration -> getName(), 0, (void *)Declaration);
#if (THUD_DBG == 1)
                Declaration -> dump();
#endif
                thud_debug("Pointer location of Decl is " << Declaration);
            }
            else if (thud_current_pass != THUD_PASS_HANDLE_INVALID_TYPE_ASSIGNMENTS)
            {
                thud_debug("This is the #" << thud_current_pass << \
                        " pass, updating FunctionDecl list with latest location info");
                thud_update_list_unique_with_decl(thud_func_list, Declaration -> getName(), 0, (void *)Declaration);
                thud_debug("Pointer location of Decl " << Declaration -> getName() << " is " << Declaration);
                return true;
            }
            return true;
        }

/* ==============================================================
 *
 * name     : VisitVarDecl()
 *
 * purpose  : Virtual function overridden for -
 *            a) Creation of static storage for each variable 
 *               of unique name
 *            b) Keeping track of each variable's type (useful for
 *               variable context save and restore before & after
 *               function call)
 *            c) Associating each function declaration with a list of
 *               variable declarations inside it (also useful for
 *               variable context save and restore before & after
 *               function call)
 *
 * ============================================================== */

        /* Using this to traverse all variable declarations & build a list of their types & sizes */
        bool VisitVarDecl (VarDecl *Declaration)
        {
            TypeSourceInfo *T = Declaration -> getTypeSourceInfo();
            //SourceRange SR = Declaration -> getSourceRange();
            data_type_details type_details;
            QualType qt = T -> getType();
            bool parmVar = false;
            bool isglobal = false;
            bool islocalstatic = false;
            std::stringstream decl_text;
            std::string varname = Declaration -> getName();
            std::string vartype = qt.getAsString();
            list<thud_decl_base>::iterator j; 
            unsigned int var_raw_loc = (Declaration -> getLocStart()).getRawEncoding(); 
            FunctionDecl *func_decl = NULL;
            std::error_code OutErrorInfo;
            SourceLocation target_source_loc = TheRewriter.getSourceMgr().getLocForStartOfFile(TheRewriter.getSourceMgr().getMainFileID());
            StringRef outName = TheRewriter.getSourceMgr().getFilename(target_source_loc);
            llvm::raw_fd_ostream outFile(outName, OutErrorInfo, llvm::sys::fs::F_None);
            Rewriter::RewriteOptions removeOpts;
            removeOpts.IncludeInsertsAtBeginOfRange = false;
            removeOpts.IncludeInsertsAtEndOfRange = false;
            removeOpts.RemoveLineIfEmpty = true;
            target_source_loc = (Declaration -> getLocEnd());

            parmVar = (Declaration -> getKind() == Decl::ParmVar) ? true:false;
            isglobal = Declaration -> hasGlobalStorage();
            islocalstatic = Declaration -> isStaticLocal();

            /* We skip parameter variables for now */
            if (parmVar)
                return true;

            /* Ensure that we build the list only in the THUD_PASS_STATIC_STORAGE_CREATION pass 
             */
            if (thud_current_pass == THUD_PASS_STATIC_STORAGE_CREATION)
            {
                type_details = thud_get_size_of_datatype(vartype, qt);
                thud_debug("Adding VarDecl to thud_var_name_list " << Declaration -> getName());
                thud_debug("Data type is " << type_details.datatype);
                thud_debug("Array size is " << type_details.array_size);
                thud_debug("Total size is " << type_details.total_size);
                thud_add_to_list_and_sublist_unique(thud_var_name_list, varname, vartype, type_details);

                if (isglobal && !islocalstatic)
                {
                    thud_debug("Is global variable " << varname << " static local ?? " << islocalstatic);
                    thud_add_list_unique_with_decl(thud_global_var_list, varname, type_details.total_size, (void *)Declaration);
                    thud_has_global = true;
                }
            }
            /* Populate FunctionDecl list with a sublist of unique variables declared there */
            else if (thud_current_pass == THUD_PASS_UPDATE_FUNCTIONLIST_WITH_VARIABLES)
            {
                thud_debug("Sorry! This is a Pass THUD_PASS_UPDATE_FUNCTIONLIST_WITH_VARIABLES stub.");

                /* We don't deal with global variables anymore ... */
                if (isglobal && !islocalstatic)
                    return true;

                /* Figure out who called this function and add the callee to the
                 * caller's list
                 */
                for(j=thud_func_list.begin(); j != thud_func_list.end(); ++j) 
                {
                    func_decl = (FunctionDecl *) (*j).location;
                    unsigned int func_raw_loc_begin = (func_decl -> getLocStart()).getRawEncoding(); 
                    unsigned int func_raw_loc_end = (func_decl -> getLocEnd()).getRawEncoding(); 
                    thud_debug("The function " << (*j).name << " lies between" << \
                            func_raw_loc_begin << " and " << func_raw_loc_end );
                    if ( var_raw_loc > func_raw_loc_begin && var_raw_loc < func_raw_loc_end)
                    {
                        thud_debug("Yippy! Found the VarDecl " << varname << \
                                " inside function " << (*j).name);
                        thud_debug("Adding it to the FuncDecl's list of VarDecls");
                        thud_add_list_unique_with_decl((*j).sublist2, varname, 0, (void *)Declaration);
                        break;
                    }
                }
            }
            else if (thud_current_pass == THUD_PASS_DECLARE_TYPE_FOR_EACH_VARIABLE)
            {
                target_source_loc = (Declaration -> getLocStart());
                thud_debug("In Pass THUD_PASS_DECLARE_TYPE_FOR_EACH_VARIABLE");

                /* We don't deal with global variables anymore ... */
                if (isglobal && !islocalstatic)
                    return true;

                thud_debug("In Pass THUD_PASS_DECLARE_TYPE_FOR_EACH_VARIABLE : Define local type member for each variable");
                /* Define a local storage to define the type for each function 
                 * local variable; this shall be used in pass THUD_PASS_SAVE_RESTORE_VARIABLE_CONTEXT while saving the
                 * context of a variable before every function call 
                 */
                thud_debug( "VarDecl full string is " << thud_decl2str(TheRewriter, Declaration));
                decl_text.str("");
                decl_text << "char localtype_" << varname << "[20] = " << "\"" << vartype << "\"" << ";\n";
                //TheRewriter.InsertText(target_source_loc.getLocWithOffset(2), decl_text.str(), true, false); 
                TheRewriter.InsertText(target_source_loc, decl_text.str(), true, false); 

                thud_update_source_with_transformations(TheRewriter);
            }

            return true;
        }
        /* DM_END 1-Jan-18 */

    private:

        Rewriter &TheRewriter;

};

class ThudPassOneConsumer:public clang::ASTConsumer
{
    public:

        explicit ThudPassOneConsumer (ASTContext * Context, Rewriter &R): Visitor (Context,R), TheRewriter(R)
        {
        }

/* ==============================================================
 *
 * name     : thud_pass_save_restore_variable_context()
 *
 * purpose  : To handle save and restore of variable's context
 *            before and after a function call. It updates the                  
 *            static storage with present data as well as present
 *            type information
 *
 * ============================================================== */

        void thud_pass_save_restore_variable_context()
        {
            std::stringstream decl_text;
            std::error_code OutErrorInfo;
            std::error_code ok;
            list<thud_decl_base>::iterator i; 
            list<thud_decl_base>::iterator j; 
            list<thud_decl_base>::iterator k; 
            //FunctionDecl *func_decl;
            std::string func_decl_str;
            int callexpr_offset = 0;
            //int func_decl_endpos = 0;
            SourceLocation target_source_loc = TheRewriter.getSourceMgr().getLocForStartOfFile(TheRewriter.getSourceMgr().getMainFileID());

            /* Take care of saving & restoring variable context before & after a function call */
            for(i=thud_func_list.begin(); i != thud_func_list.end(); ++i) 
            {

                /* Save context of each variable before each function call */
                for(j=(i->sublist).begin(); j != (i->sublist).end(); ++j) 
                {
                    CallExpr *CE = (CallExpr *) j->decl;
                    thud_debug("\tCallExpr seen here was " << j->name);

                    callexpr_offset = thud_callexpr2str(TheRewriter, CE);

                    for (k = i->sublist2.begin(); k != (i->sublist2).end(); ++k)
                    {
                        target_source_loc = (CE -> getLocStart()); 
                        //TypeSourceInfo *T = Declaration -> getTypeSourceInfo();
                        //QualType qt = T -> getType();
                        //std::string vartype = qt.getAsString();

                        thud_debug("\t\tVariable handled by parent function is " << k->name);
                        decl_text.str("");

                        /* Do a memcpy of the local variable to its static storage counterpart */
                        decl_text << "memcpy (static_" << k->name << ", " << "&" << k->name << ",sizeof(" << k->name << "));\n";
                        /* Update the type of the variable */
                        //decl_text << "strncpy (type_" << k->name << ", \"" << vartype << "\", 20);\n";
                        decl_text << "strncpy (type_" << k->name << ", localtype_" << k->name << ", 20);\n";
                        TheRewriter.InsertText(target_source_loc.getLocWithOffset(callexpr_offset), decl_text.str(), true, false);

                        target_source_loc = (CE -> getLocEnd()); 
                        decl_text.str("");
                        decl_text << "strncpy (type_" << k->name << ", \"Invalid\", 20);\n";
                        TheRewriter.InsertText(target_source_loc.getLocWithOffset(2), decl_text.str(), true, false);
                    }
                }
            }

            target_source_loc = TheRewriter.getSourceMgr().getLocForStartOfFile(TheRewriter.getSourceMgr().getMainFileID());
            decl_text.str("");
            decl_text << "\n#include <stdio.h>";
            decl_text << "\n#include <string.h>";
            decl_text << "\n#include <stdlib.h>\n";
            TheRewriter.InsertText(target_source_loc, decl_text.str(), true, false);
            thud_update_source_with_transformations(TheRewriter);
        }

/* ==============================================================
 *
 * name     : thud_sourcetx_handle_undeclared_instances_incrementally()
 *
 * purpose  : To handle undeclared variables in the program   
 *            in per-variable manner (one pass / variable) by replicating               
 *            the function body for each type associated with the 
 *            variable.
 *
 * ============================================================== */

        void thud_sourcetx_handle_undeclared_instances_incrementally()
        {
            std::stringstream decl_text;
            std::error_code OutErrorInfo;
            std::error_code ok;
            list<thud_decl_base>::iterator i; 
            list<thud_decl_base>::iterator j; 
            list<thud_decl_base>::iterator k; 
            std::string func_decl_str;
            int func_decl_startpos;
            int func_decl_endpos;
            int varname_pos = 0;
            std::string func_sub_body;
            std::string varname;
            std::string funcname;
            FunctionDecl *func_decl = NULL;
            thud_decl_base *var_type_list = NULL;
            thud_decl_base *func_block = NULL;
            SourceLocation target_source_loc = TheRewriter.getSourceMgr().getLocForStartOfFile(TheRewriter.getSourceMgr().getMainFileID());
            Rewriter::RewriteOptions removeOpts;
            removeOpts.IncludeInsertsAtBeginOfRange = false;
            removeOpts.IncludeInsertsAtEndOfRange = false;
            removeOpts.RemoveLineIfEmpty = true;
            thud_debug("Going to do some michief...");

            if (thud_undeclared_var_name_list_pruned.empty())
            {
                thud_debug("Handled all undeclared variables");
                handled_all_undeclared_vars = true;
                thud_update_source_with_transformations(TheRewriter);
                return;
            }

            /* Handle the next undeclared variable */

            /* Remove the function name prefix along with underscore */
            j=thud_undeclared_var_name_list_pruned.begin(); 
            varname_pos = (j->name).find_last_of("_");
            funcname = (j->name).substr(0,varname_pos);
            varname = (j->name).substr(varname_pos+1);

            thud_debug("Next variable to be handled is " << varname);
            thud_debug("Function inside which it is undeclared is " << funcname);

            func_block = thud_retrieve_attr_block(thud_func_list, funcname);

            decl_text.str("");
            //func_decl = (FunctionDecl *) (*j).location;
            func_decl = (FunctionDecl *) func_block->decl;

            thud_debug ("FuncDecl ptr is : " << func_decl);
            thud_debug ("Dumping function decl here :");
#if (THUD_DBG == 1)
            func_decl -> dump();
#endif
            /* First clear off the original body of the function */
            func_decl_str = thud_decl2str(TheRewriter, (VarDecl *) func_decl);
            func_decl_startpos = func_decl_str.find_first_of("{");
            func_decl_endpos = func_decl_str.find_last_of("}");
            func_sub_body = func_decl_str.substr(func_decl_startpos+1, func_decl_endpos-func_decl_startpos-2);

            target_source_loc = func_decl -> getLocStart();

            TheRewriter.RemoveText(target_source_loc.getLocWithOffset(func_decl_startpos+1), (func_decl_endpos - func_decl_startpos - 1), removeOpts);

            thud_debug("Varname is " << varname);

            var_type_list = thud_retrieve_attr_block(thud_var_name_list, varname);

            thud_debug("Sub-body is " << func_sub_body);

            /* Now comes the tricky part ! Need to create multiple definitions of the same
             * variable name (if it exists) within the function where it was not defined 
             * and replicate multiple blocks of the original code saved in func_sub_body 
             */

            /* Handle the case where the variable 'x' is not existing anymore ! */
            //decl_text << "\nif (type_" << (*i).name << " == 'n')\n{" 
            decl_text << "\nif (strncmp(type_" << varname << ",\"Invalid\","<< "20)==0)\n{"\
                << "printf (\"Sorry, the variable " << varname << " doesn't exist here !\");"  \
                <<  "\nexit(1);\n}\n";
            TheRewriter.InsertText(target_source_loc.getLocWithOffset(func_decl_startpos+1), decl_text.str(), true, false);

            for (k = (var_type_list -> sublist).begin(); k != (var_type_list -> sublist).end(); k++)
            {
                bool is_array = false;
                decl_text.str("");
                size_t pos = 0;
                std::string type_name = k->name;

                decl_text << "\nif (strncmp(type_" << varname << ", \""<< k->name << "\",20)==0)\n{";

                /* If it's an array, insert name of the variable to the type string */
                pos = (type_name).find_first_of("[");
                if (string::npos != pos)
                {
                    (type_name).insert(pos,varname); 
                    is_array = true;
                }

                /* Declare the variable according to the type */
                if (is_array)
                    decl_text << type_name << ";\n";
                else
                    decl_text << type_name << " " << varname << ";\n";

                /* Restore its context from static storage */
                decl_text << "memcpy (&" << varname << ", " << "static_" << varname << ", " << "sizeof (" << varname\
                    << "));\n";
                decl_text << func_sub_body << "}\n";
                TheRewriter.InsertText(target_source_loc.getLocWithOffset(func_decl_startpos+1), decl_text.str(), true, false);
            }

            thud_update_source_with_transformations(TheRewriter);
            thud_undeclared_var_name_list_pruned.pop_front();
        }

/* ==============================================================
 *
 * name     : thud_sourcetx_prune_undeclared_variables()
 *
 * purpose  : To create a list one-one mappings between functions
 *            and variables not declared. This is to avoid redundant
 *            instances of the Diagnostic handler reporting variables
 *            not declared multiple times within the same function.
 *            This helps us to handle the variables not declared only
 *            once for each function where they were not declared.
 *
 * ============================================================== */

        void thud_sourcetx_prune_undeclared_variables()
        {
            std::stringstream decl_text;
            std::error_code OutErrorInfo;
            std::error_code ok;
            list<thud_decl_base>::iterator i; 
            list<thud_decl_base>::iterator j; 
            list<thud_decl_base>::iterator k; 
            std::string func_decl_str;
            SourceLocation *var_SL = NULL;
            std::string func_sub_body;
            FunctionDecl *func_decl = NULL;
            thud_decl_base *var_type_list = NULL;
            Rewriter::RewriteOptions removeOpts;
            removeOpts.IncludeInsertsAtBeginOfRange = false;
            removeOpts.IncludeInsertsAtEndOfRange = false;
            removeOpts.RemoveLineIfEmpty = true;

            /* Find out in which function each undeclared variable is located & prune out 
             * unique declared instances
             */
            for(i=thud_undeclared_var_name_list.begin(); i != thud_undeclared_var_name_list.end(); ++i) 
            {
                //decl_text.str("\nMuhahaha!\n");
                var_SL = (SourceLocation *) (*i).location;
                //TheRewriter.InsertText(*SL, decl_text.str(), true, false);
                unsigned int undef_var_raw_loc = var_SL -> getRawEncoding();

                thud_debug("The undeclared variable " << (*i).name << " is currently located @" << undef_var_raw_loc );

                var_type_list = thud_retrieve_attr_block(thud_var_name_list, i->name);

                if (NULL == var_type_list)
                {
                    thud_error("This undeclared variable " << (*i).name << " was not defined anywhere else in the source !!");
                    premature_exit = true;
                    return;
                }

                /* First, figure out in which function this undeclared variable was discovered */
                for(j=thud_func_list.begin(); j != thud_func_list.end(); ++j) 
                {
                    func_decl = (FunctionDecl *) (*j).location;
                    unsigned int func_raw_loc_begin = (func_decl -> getLocStart()).getRawEncoding(); 
                    unsigned int func_raw_loc_end = (func_decl -> getLocEnd()).getRawEncoding(); 
                    thud_debug("The function " << (*j).name << " lies between" << func_raw_loc_begin << " and " << func_raw_loc_end );
                    thud_debug("Pointer location of Decl is " << func_decl);

                    if ( undef_var_raw_loc > func_raw_loc_begin && undef_var_raw_loc < func_raw_loc_end)
                    {
                        thud_debug("Yippy! Found the undeclared variable " << (*i).name << " inside function " << (*j).name);
                        thud_add_list_unique_with_decl(thud_undeclared_var_name_list_pruned, (j->name) + "_" + i->name, 0, (void *)j->location);
                        break;
                    }
                }
            }

            thud_debug("Displaying the pruned list of undeclared variables");
            thud_display_attr_details(thud_undeclared_var_name_list_pruned);

        }

/* ==============================================================
 *
 * name     : thud_sourcetx_static_storage_creation()
 *
 * purpose  : To create static storage for each uniquely named variable
 *            of maximum size (across all its various declarations) 
 *
 * ============================================================== */

        void thud_sourcetx_static_storage_creation()
        {
            std::stringstream decl_text;
            std::error_code OutErrorInfo;
            std::error_code ok;
            list<thud_decl_base>::iterator i; 
            list<thud_decl_base>::iterator j; 
            list<thud_decl_base>::iterator k; 
            FunctionDecl *main_decl = NULL;
            SourceLocation target_source_loc = TheRewriter.getSourceMgr().getLocForStartOfFile(TheRewriter.getSourceMgr().getMainFileID());
            Rewriter::RewriteOptions removeOpts;
            removeOpts.IncludeInsertsAtBeginOfRange = false;
            removeOpts.IncludeInsertsAtEndOfRange = false;
            removeOpts.RemoveLineIfEmpty = true;

            /* Define static storage here... */
            //decl_text.str("");
            for(i=thud_var_name_list.begin(); i != thud_var_name_list.end(); ++i) //cout << (*i).name << " "; // print member
            {
                /* We do this sanity check to ensure that we're not defining
                 * any weird variables derived from standard header files like
                 * "_IO_2_1_stdout_" or "stdin" */
                if ((*i).size != 0)
                {
                    decl_text << "static char static_" << (*i).name << "[" << (*i).size << "]; /* THUD! Pass THUD_PASS_STATIC_STORAGE_CREATION */\n"; 
                    //decl_text << "static char " << "type_"<<(*i).name << " = 'n'; /* THUD! Pass THUD_PASS_STATIC_STORAGE_CREATION */\n"; 
                    decl_text << "static char " << "type_"<<(*i).name << "[20] = \"Invalid\"; /* THUD! Pass THUD_PASS_STATIC_STORAGE_CREATION */\n"; 
                }
            }
            TheRewriter.InsertText(target_source_loc, decl_text.str(), true, true);

            if (thud_has_global == true)
            {
                /* Relocate global variable definitions to main() */
                /* First, get the main() function's Decl */
                for(i=thud_func_list.begin(); i != thud_func_list.end(); ++i) 
                {
                    FunctionDecl *temp = (FunctionDecl *)((*i).decl);

                    if (NULL == temp)
                        continue;

                    if ((*i).name == "main" || temp->isMain())
                    {
                        thud_debug("Found main() !!");
                        main_decl = (FunctionDecl *) (*i).decl;
                        break;
                    }

                    thud_debug("Function seen here is " << (*i).name << "\n");
                    thud_debug("Pointer location of Declaration is " << temp << "\n");
                    thud_debug("Decl seen here is " << temp->getName() << "\n");
                }
                /* If main() function's Decl is present, start global variable relocation */
                if (main_decl != NULL)
                {
                    /* Get start location of main()'s '{' token so that we can insert
                     * the global variable definitions here
                     */
                    target_source_loc = main_decl -> getLocStart();
                    std::string main_decl_str = thud_decl2str(TheRewriter, (VarDecl *) main_decl);
                    int main_decl_strsize = main_decl_str.find_first_of("{");
                    thud_debug("main() declaration statement is " << main_decl_str);
                    thud_debug("Size of main() declaration statement is " << main_decl_strsize);

                    /* Redefine global variables inside main()  */
                    for(i=thud_global_var_list.begin(); i != thud_global_var_list.end(); ++i) 
                    {
                        /* Insert the global variable definitions within main() */
                        decl_text.str("");
                        decl_text << thud_decl2str(TheRewriter, (VarDecl *) (*i).decl);
                        decl_text << "; /* THUD! Pass THUD_PASS_STATIC_STORAGE_CREATION */\n";
                        //TheRewriter.InsertText(target_source_loc, decl_text.str(), true, true);
                        TheRewriter.InsertText(target_source_loc.getLocWithOffset(main_decl_strsize+2), decl_text.str(), true, false);
                    }   

                    /* Remove original global variable definitions  */
                    for(i=thud_global_var_list.begin(); i != thud_global_var_list.end(); ++i) 
                    {
                        VarDecl *global_var_decl = NULL;
                        int global_vardecl_strsize = 0;
                        std::string global_var_string;

                        /* Remove the original global variable definitions */
                        global_var_decl = (VarDecl *)(*i).decl;
                        target_source_loc = global_var_decl -> getLocStart();
                        global_var_string = thud_decl2str(TheRewriter, (VarDecl *) (*i).decl);
                        global_vardecl_strsize = global_var_string.size();
                        //global_vardecl_strsize++; // To eliminate ';' at the end of variable definition
                        target_source_loc = global_var_decl -> getLocStart();
                        thud_debug("Global var decl string for removal is " << global_var_string);
                        thud_debug("Size of global var decl string is " << global_vardecl_strsize);
                        TheRewriter.RemoveText(target_source_loc, global_vardecl_strsize, removeOpts);
                    }
                }
                thud_has_global = false;
            }

            thud_update_source_with_transformations(TheRewriter);
        }

/* ==============================================================
 *
 * name     : thud_sourcetx_handle_invalid_types()
 *
 * purpose  : This function handles type mismatch issues in the program,
 *            particularly with struct and array types; which includes
 *            invalid assignment issues and invalid type reference 
 *            issues.
 *
 * ============================================================== */

        void thud_sourcetx_handle_invalid_types()
        {
            std::stringstream decl_text;
            std::error_code OutErrorInfo;
            std::error_code ok;
            list<thud_decl_base>::iterator i; 
            SourceLocation *SL = NULL;

            if (thud_invalid_type_list.empty())
            {
                std::error_code OutErrorInfo;
#if (THUD_DBG == 1)
                SourceLocation target_source_loc = TheRewriter.getSourceMgr().getLocForStartOfFile(TheRewriter.getSourceMgr().getMainFileID());
                thud_debug("Writing to file : " << TheRewriter.getSourceMgr().getFilename(target_source_loc).data());
#endif
                thud_update_source_with_transformations(TheRewriter);
                return;
            }

            for(i=thud_invalid_type_list.begin(); i != thud_invalid_type_list.end(); ++i) 
            {
                SL = (SourceLocation *) i->location;
                thud_debug("Invalid type is " << i->name);
                thud_replace_invalid_text(TheRewriter, SL);
            }
        }

/* ==============================================================
 *
 * name     : HandleTranslationUnit()
 *
 * purpose  : This function orchestrates what actions to perform        
 *            besides AST traversal. The actions to be done after     
 *            AST traversal depend on which compiler pass THUD is  
 *            currently executing. There are totally 8 distinct types 
 *            of compiler pass states as of now.
 *
 * ============================================================== */

        void HandleTranslationUnit (clang::ASTContext & Context) override
        {
            thud_debug("Running #" << thud_current_pass << " compiler pass\n");
            Visitor.TraverseDecl (Context.getTranslationUnitDecl ());
            thud_debug("Displaying set of collected Variable Info");
            thud_display_attr_details(thud_var_name_list);
            thud_debug("Displaying Global variable Info");
            thud_display_attr_details(thud_global_var_list);
            thud_debug("Displaying Function decl Info");
            thud_display_attr_details(thud_func_list);
            thud_debug("Displaying list of undeclared variables");
            thud_display_attr_details(thud_undeclared_var_name_list);
            thud_debug("Displaying the pruned list of undeclared variables");
            thud_display_attr_details(thud_undeclared_var_name_list_pruned);


            if (thud_current_pass == THUD_PASS_STATIC_STORAGE_CREATION)
            {
                thud_printf("##########################################################################");
                thud_printf(" Gathered var info, going to create static storage & relocate globals to main()");
                thud_printf("##########################################################################");
                thud_sourcetx_static_storage_creation();
            }
            else if (thud_current_pass == THUD_PASS_PRUNE_UNDECLARED_VARIABLES)
            {
                thud_printf("########################################");
                thud_printf(" Going to prune undeclared variables");
                thud_printf("########################################");
                thud_sourcetx_prune_undeclared_variables();
            }
            else if (thud_current_pass == THUD_PASS_HANDLE_UNDECLARED_VARIABLES)
            {
                thud_printf("####################################");
                thud_printf(" Going to handle undeclared variables");
                thud_printf("####################################");
                thud_sourcetx_handle_undeclared_instances_incrementally();
            }
            else if (thud_current_pass == THUD_PASS_UPDATE_FUNCTIONLIST_WITH_VARIABLES)
            {
                thud_printf("#####################################################################");
                thud_printf(" Populated functiondecl list with a sublist of variables handled"); 
                thud_printf(" not doing anything else here..."); 
                thud_printf("####################################################################");
            }
            else if (thud_current_pass == THUD_PASS_DECLARE_TYPE_FOR_EACH_VARIABLE)
            {
                thud_printf("##########################################################################");
                thud_printf(" Declared a type member for each variable declared inside each function"); 
                thud_printf(" not doing anything else here..."); 
                thud_printf("##########################################################################");
            }
            else if (thud_current_pass == THUD_PASS_UPDATE_FUNCTION_LOCATION_INFO)
            {
                thud_printf("############################################################");
                thud_printf(" Waited for each function's source location to be updated"); 
                thud_printf(" not doing anything else here..."); 
                thud_printf("############################################################");
            }
            else if (thud_current_pass == THUD_PASS_SAVE_RESTORE_VARIABLE_CONTEXT)
            {
                thud_printf("############################################################################");
                thud_printf(" Going to handle save & restore of variable context during function call");
                thud_printf("############################################################################");
                thud_pass_save_restore_variable_context();
            }
            else if (thud_current_pass == THUD_PASS_HANDLE_INVALID_TYPE_ASSIGNMENTS)
            {
                thud_printf("##################################################");
                thud_printf(" Going to handle invalid type assignments, if any");
                thud_printf("##################################################");
                thud_sourcetx_handle_invalid_types();
            }
        }

    private:
        ThudPassOneVisitor Visitor;
        Rewriter &TheRewriter;

};

class ThudPassOneAction: public clang::ASTFrontendAction
{
    public:

        void EndSourceFileAction() override 
        {
            /* Write to the original file */
            if ((thud_current_pass == THUD_PASS_SAVE_RESTORE_VARIABLE_CONTEXT) || \
                    (thud_current_pass == THUD_PASS_STATIC_STORAGE_CREATION) || \
                    (thud_current_pass == THUD_PASS_HANDLE_UNDECLARED_VARIABLES) || \
                    (thud_current_pass == THUD_PASS_HANDLE_INVALID_TYPE_ASSIGNMENTS))
                return;

            thud_update_source_with_transformations(TheRewriter);
        }

        virtual std::unique_ptr <clang::ASTConsumer > CreateASTConsumer (clang::CompilerInstance & Compiler,
                llvm::StringRef InFile) override
        {
            TheRewriter.setSourceMgr(Compiler.getSourceManager(), Compiler.getLangOpts());
            return std::unique_ptr < clang::ASTConsumer >
                (new ThudPassOneConsumer (&Compiler.getASTContext (), TheRewriter));
        }

    private:

        Rewriter TheRewriter;

};

/* ============================================================== */

/* =====
 * Main  
 * =====
 */

/* Apply a custom category to all command-line options so that they are the
 * only ones displayed.
 */
static llvm::cl::OptionCategory MyToolCategory("thud-dynamic-tool options");

/* CommonOptionsParser declares HelpMessage with a description of the common
 * command-line options related to the compilation database and input files.
 * It's nice to have this help message in all tools.
 */
static cl::extrahelp CommonHelp(CommonOptionsParser::HelpMessage);

/* A help message for this specific tool can be added afterwards.
 */
static cl::extrahelp MoreHelp("\nMore help text...");

int thud_run_tool(CommonOptionsParser &OptionsParser)
{
    int ret = 0;
    ClangTool Tool(OptionsParser.getCompilations(),
            OptionsParser.getSourcePathList());
    ret = Tool.run(newFrontendActionFactory<ThudPassOneAction>().get());
    return ret;
}

/* ==============================================================
 *
 * name     : main()
 *
 * purpose  : This is the beginning of the whole program, which         
 *            each compiler pass one after the other to generate
 *            the final source file with modifications for 
 *            dynamic scoping of variables.
 *
 * ============================================================== */

int main(int argc, const char **argv) {

    CommonOptionsParser OptionsParser(argc, argv, MyToolCategory);

    int passes[10] = 
    {
        THUD_PASS_STATIC_STORAGE_CREATION,
        THUD_PASS_PRUNE_UNDECLARED_VARIABLES,
        THUD_PASS_HANDLE_UNDECLARED_VARIABLES,
        THUD_PASS_UPDATE_FUNCTIONLIST_WITH_VARIABLES,
        THUD_PASS_DECLARE_TYPE_FOR_EACH_VARIABLE,
        THUD_PASS_UPDATE_FUNCTION_LOCATION_INFO,
        THUD_PASS_SAVE_RESTORE_VARIABLE_CONTEXT,
        THUD_PASS_HANDLE_INVALID_TYPE_ASSIGNMENTS,
        THUD_PASS_END
    };

    for (int i = 0; passes[i] != THUD_PASS_END ; )
    {
        thud_current_pass = passes[i];
        thud_run_tool(OptionsParser);
        
        thud_printf("Running pass " << passes[i]);

        if ((passes[i] == THUD_PASS_HANDLE_UNDECLARED_VARIABLES) && (!handled_all_undeclared_vars))
        {
            thud_printf("HANDLING UNDECLARED VARS ...");
            /* Do nothing, stay here in this state till all
             * undeclared variables are handled
             */
            if (premature_exit)
            {
                thud_error("Sorry! THUD found an undeclared variable, exiting prematurely...");
                return 1;
            }
        }
        else
        {
            i++;
        }
    }
    return 0;
}

/* ============================================================== */
