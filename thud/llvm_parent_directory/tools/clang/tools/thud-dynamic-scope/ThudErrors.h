/* ============================================================== */

/*
 *     <>=======()
 *    (/\___   /|\\          ()==========<>_
 *          \_/ | \\        //|\   ______/ \)
 *            \_|  \\      // | \_/
 *              \|\/|\_   //  /\/
 *               (oo)\ \_//  /
 *              //_/\_\/ /  |
 *             @@/  |=\  \  |
 *                  \_=\_ \ |
 *                    \==\ \|\_ 
 *   THUD!         __(\===\(  )\
 *   ~~~~~        (((~) __(_/   |
 *                     (((~) \  /
 *                     ______/ /
 *                     '------'
 *
 */

/* ==============================================================
 *
 *  filename : ThudErrors.h 
 *  purpose  : Contains all constant definitions for different
 *             types of errors encountered by the program.
 *             Also has useful API for easy debugging.
 *
 * ============================================================== */

#ifndef __THUD_ERRORS_H
#define __THUD_ERRORS_H

/* ============================================================== */

#include <utility>

#define THUD_ASSERT(var) if (var) { thud_error("Sorry, THUD! encountered a fatal error, shall quit!"); return 1;}

#define THUD_DBG       0

#if (THUD_DBG == 1)
#define thud_debug(args) llvm::errs() << "THUD! DEBUG (" << __FUNCTION__ << "," << __LINE__ << ") :" << args << "\n";
#else
#define thud_debug(...)  
#endif

#define thud_printf(args) llvm::errs() << "THUD! (" << __FUNCTION__ << "," << __LINE__ << ") :" << args << "\n";
#define thud_error(args) llvm::errs() << "THUD! ERR - (" << __FUNCTION__ << "," << __LINE__ << ") :" << args << "\n";

enum thud_errors
{
    THUD_ZERO_SIZE = 0,
    THUD_FAILURE = 0,
    THUD_SUCCESS,
    THUD_OUT_OF_MEM,
    THUD_DUPLICATE_ENTRY,
    THUD_NODE_NOT_FOUND
};

#endif

