struct B
{
    int c[2];
};

struct A
{
    int x;
    int y;
    struct B m;
};

void
f1 ()
{
  printf ("a[4].x is %d\n", a[4].x);
  printf ("a[4].y is %d\n", a[4].y);
  printf ("a[0].m.c[0] is %d\n", a[0].m.c[0]);
  printf ("a[0].m.c[1] is %d\n", a[0].m.c[1]);
}

int
main ()
{
  struct A a[5] = {{9,8,{1,2}},{8,7,{2,3}},{7,6,{3,4}},{6,5,{4,5}},{5,4,{5,6}}};
  f1 ();
  return 0;
}
