int sum_it()
{
    static int sum = 0;
    int ret = 0;
    int temp = 0;

    if (n <= 0)
    {
        sum = sum + n;
        temp = sum;
        sum = 0;
        return temp;
    }
    else
    {
        sum = sum + n;
        n = n-1;
        ret = sum_it();
        return ret;
    }
}

int f1()
{
    float n = 10.5;
    int ret = 0;
    ret = sum_it();
    return ret;
}

int f2()
{
    int n = 10.5;
    int ret = 0;
    ret = sum_it();
    return ret;
}

int main()
{
    int ret = 0;
    ret = f1();
    printf("Value returned by f1 is %d\n", ret);
    ret = f2();
    printf("Value returned by f2 is %d\n", ret);
}
