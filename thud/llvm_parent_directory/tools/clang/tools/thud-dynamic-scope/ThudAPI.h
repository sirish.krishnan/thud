/* ============================================================== */

/*
 *     <>=======()
 *    (/\___   /|\\          ()==========<>_
 *          \_/ | \\        //|\   ______/ \)
 *            \_|  \\      // | \_/
 *              \|\/|\_   //  /\/
 *               (oo)\ \_//  /
 *              //_/\_\/ /  |
 *             @@/  |=\  \  |
 *                  \_=\_ \ |
 *                    \==\ \|\_ 
 *   THUD!         __(\===\(  )\
 *   ~~~~~        (((~) __(_/   |
 *                     (((~) \  /
 *                     ______/ /
 *                     '------'
 *
 */

/* ==============================================================
 *
 *  filename : ThudAPI.h 
 *  purpose  : Contains all class & function declarations for  
 *             Linked List functionality required for Thud.
 *
 * ============================================================== */

#ifndef __THUD_API_H
#define __THUD_API_H

/* ============================================================== */

/* ================
 * Include file(s) 
 * ================
 */

#include <iostream>
#include <list>
#include "ThudErrors.h"
using namespace std;

/* ============================================================== */

/* ===================
 * Enum definition(s) 
 * ===================
 */

enum datatypes
{
    THUD_INVALID=0,
    THUD_FLOAT,
    THUD_FLOAT_ARRAY,
    THUD_INT,
    THUD_INT_ARRAY,
    THUD_STRUCT,
    THUD_STRUCT_ARRAY
};


/* ============================================================== */

/* ====================
 * Class declaration(s) 
 * ====================
 */

class thud_decl_base
{
    friend ostream &operator<<(ostream &, const thud_decl_base &);

    public:
    std::string name;               // Name of key attribute 
    int type;                       // Type of key attribute 
    int size;                       // Size of the attribute
    void *decl;                     // Pointer to its Decl object
    void *location;                 // Pointer to its location in source file 
    list<thud_decl_base> sublist;   // Sublist of associated attributes
    list<thud_decl_base> sublist2;  // Sublist 2 of associated attributes

    // Constructor & Destructor definitions
    thud_decl_base();
    thud_decl_base(const thud_decl_base &);
    thud_decl_base(std::string attr_name, int attr_size, void *attr_data, void* attr_loc);
    ~thud_decl_base(){};

    // Operator overload function declarations
    thud_decl_base &operator=(const thud_decl_base &rhs);
    int operator==(const thud_decl_base &rhs) const;
    int operator<(const thud_decl_base &rhs) const;

    // Function declarations
    int get_size(int);
    int thud_update_attr_details(std::string attr_name, int attr_size, void *decl, void *attr_loc);
};

typedef struct data_type_details
{
    int datatype;
    int array_size;
    int total_size;
} data_type_details;

enum thud_pass_numbers
{
    THUD_PASS_STATIC_STORAGE_CREATION = 1,
    THUD_PASS_PRUNE_UNDECLARED_VARIABLES,
    THUD_PASS_HANDLE_UNDECLARED_VARIABLES,
    THUD_PASS_UPDATE_FUNCTIONLIST_WITH_VARIABLES,
    THUD_PASS_DECLARE_TYPE_FOR_EACH_VARIABLE,
    THUD_PASS_UPDATE_FUNCTION_LOCATION_INFO,
    THUD_PASS_SAVE_RESTORE_VARIABLE_CONTEXT,
    THUD_PASS_HANDLE_INVALID_TYPE_ASSIGNMENTS,
    THUD_PASS_END
};

/* ============================================================== */

/* ===================
 * Function prototypes 
 * ===================
 */

int test_linked_list();
thud_decl_base* thud_retrieve_attr_block(list<thud_decl_base> &L, std::string attr_name);
void thud_display_attr_details(list<thud_decl_base> &L);
int thud_add_to_list_unique(list<thud_decl_base> &L, std::string attr_name, int attr_size);
int thud_add_list_unique_with_decl(list<thud_decl_base> &L, std::string attr_name, int attr_size, void *ptr);
int thud_update_list_unique_with_decl(list<thud_decl_base> &L, std::string attr_name, int attr_size, void *ptr);
int thud_get_type_of_variable(std::string attr_type);
//int thud_add_to_list_and_sublist_unique(list<thud_decl_base> &L, std::string attr_name, std::string attr_type, int attr_size);
int thud_add_to_list_and_sublist_unique(list<thud_decl_base> &L, std::string attr_name, std::string attr_type, data_type_details type_details);
int thud_add_to_list_notunique(list<thud_decl_base> &L, std::string attr_name, void *data);
data_type_details thud_get_size_of_datatype (std::string attr_type, clang::QualType &qt);
std::string thud_decl2str(clang::Rewriter &, clang::Decl *d);
int thud_callexpr2str(clang::Rewriter &TheRewriter, clang::CallExpr *d);
int thud_replace_invalid_text(clang::Rewriter &TheRewriter, clang::SourceLocation *SL);
int thud_get_size_of_struct( std::string attr_type, clang::QualType qt );
int thud_get_array_size(std::string attr_type);
#endif
