# DM_START 1-Jan-18 : Compile all these examples by default
#if(NOT CLANG_BUILD_EXAMPLES)
#  set_property(DIRECTORY PROPERTY EXCLUDE_FROM_ALL ON)
#  set(EXCLUDE_FROM_ALL ON)
#endif()

#if(CLANG_ENABLE_STATIC_ANALYZER)
add_subdirectory(analyzer-plugin)
#endif()
# DM_END 1-Jan-18
add_subdirectory(clang-interpreter)
add_subdirectory(PrintFunctionNames)
add_subdirectory(AnnotateFunctions)
