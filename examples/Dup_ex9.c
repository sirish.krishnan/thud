struct A
{
    int x;
    int y;
};

void f1()
{
    printf("a.x is %d\n", a.x);
    printf("a.y is %d\n", a.y);
}

void f2()
{
    printf("a[3] is %d\n", a[3]);
}

void g1()
{
    struct A a;
    a.x = 10;
    a.y = 7;
    f1();
}

void g2()
{
    int a[5] = {1,2,3,4,5};
    f2();
}

int main()
{
    g1();
    g2();
    return 0;
}
