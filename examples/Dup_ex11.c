struct A
{
    int x;
    int y;
};

void f1()
{
    printf("a[1] is %d\n", a[1]);
    printf("a[2] is %d\n", a[2]);
}

void f2()
{
    printf("a.x is %d\n", a.x);
    printf("a.y is %d\n", a.y);
}

void g1()
{
    a.x = 10;
    a.y = 11;
}

void g2()
{
    a[1] = 11;
    a[2] = 12;
    f1();
}

void g3()
{
    struct A a = {8,9};
    f2();
}

int main()
{
    int a[4] = {1,2,3,4};
    //g1();
    g2();
    g3();
    f1();
}
