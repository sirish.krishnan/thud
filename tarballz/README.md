Download & stick the following files to this folder :

CFE v5.0.1 :
http://releases.llvm.org/5.0.1/cfe-5.0.1.src.tar.xz

Clang tools extra V5.0.1 :
http://releases.llvm.org/5.0.1/clang-tools-extra-5.0.1.src.tar.xz

LLVM v5.0.1 :
http://releases.llvm.org/5.0.1/llvm-5.0.1.src.tar.xz
