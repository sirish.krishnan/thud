THUD_BIN="$PWD/../llvm-5.0.1.src/build/bin/thud-dynamic-scope"
THUD_EXAMPLES_DIR="$PWD/../examples"

cd $THUD_EXAMPLES_DIR

for i in {1..11}
do 
    cp Dup_ex$i.c ex$i.c
    $THUD_BIN ex$i.c --
    indent ex$i.c
    gcc ex$i.c -o ex$i
done
