TARBALLZ_DIR="$PWD/../tarballz/"
HOME_DIR="$PWD/../"

# Untar LLVM, Clang & extra tools
cd $HOME_DIR
tar -xvf $TARBALLZ_DIR/cfe-5.0.1.src.tar.xz
tar -xvf $TARBALLZ_DIR/llvm-5.0.1.src.tar.xz
tar -xvf $TARBALLZ_DIR/clang-tools-extra-5.0.1.src.tar.xz

# Move them to the correct directories
mv $HOME_DIR/cfe-5.0.1.src/ $HOME_DIR/llvm-5.0.1.src/tools/clang
mv $HOME_DIR/clang-tools-extra-5.0.1.src/ $HOME_DIR/llvm-5.0.1.src/tools/clang/tools/clang-tools-extra

# Copy THUD! along with supporting Cmakefile changes
cp -r $HOME_DIR/thud/llvm_parent_directory/* $HOME_DIR/llvm-5.0.1.src/

# Configure build directory & options
cd $HOME_DIR/llvm-5.0.1.src/
mkdir build && cd build
cmake -G Ninja ../ -DLLVM_BUILD_TESTS=ON -DCMAKE_BUILD_TYPE=Release

# Start building!
ninja
